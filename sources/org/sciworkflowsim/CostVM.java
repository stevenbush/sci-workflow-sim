package org.sciworkflowsim;

import org.cloudbus.cloudsim.CloudletScheduler;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.core.CloudSim;
import org.workflowsim.ClusterStorage;
import org.workflowsim.WorkflowSimTags;
import org.workflowsim.utils.ReplicaCatalog;
import org.workflowsim.utils.ReplicaCatalog.FileSystem;

/**
 * Cost is extended from VM class, it different from VM in that the usage of this kind of VM is billed by billing cycle.
 * If there is not enough budget for the next billing cycle, this VM will be terminated.
 * 
 * @author stevenbush (shi_jiyuan@outlook.com)
 * @version sci-workflow-sim 1.0
 * @since 201412
 */
public class CostVM extends Vm {

	/*
	 * The local storage system a vm has if file.system=LOCAL
	 */
	private ClusterStorage storage;
	/*
	 * The state of a vm. It should be either WorkflowSimTags.VM_STATUS_IDLE or VM_STATUS_READY (not used in
	 * workflowsim) or VM_STATUS_BUSY
	 */
	private int state;

	/**
	 * the cost of using memory in this resource
	 */
	private double costPerMem = 0.0;

	/**
	 * the cost of using bandwidth in this resource
	 */
	private double costPerBW = 0.0;

	/**
	 * the cost of using storage in this resource
	 */
	private double costPerStorage = 0.0;

	/**
	 * the cost of using CPU in this resource
	 */
	private double cost = 0.0;

	/**
	 * the billing cycle of this VM
	 */
	private long billing_cycle = 0;

	/**
	 * the start time of this VM
	 */
	private double vm_start_time = 0;

	/**
	 * the end time of this VM
	 */
	private double vm_end_time = 0;

	/**
	 * the previous billing time of this VM
	 */
	private double previous_billing_time = 0;

	/**
	 * Creates the CostVM object
	 * 
	 * @param id
	 *            unique ID of the VM
	 * @param userId
	 *            ID of the VM's owner
	 * @param mips
	 *            the mips
	 * @param numberOfPes
	 *            amount of CPUs
	 * @param ram
	 *            amount of ram
	 * @param bw
	 *            amount of bandwidth
	 * @param size
	 *            amount of storage
	 * @param vmm
	 *            virtual machine monitor
	 * @param cloudletScheduler
	 *            the scheduler for scheduling the cloudlet in this VM
	 * @param costPerMem
	 *            cost for using memory
	 * @param costPerBW
	 *            cost for using bandwidth
	 * @param costPerStorage
	 *            cost for using storage
	 * @param cost
	 *            cost for CPU
	 * @param billing_cycle
	 *            the billing cycle for this VM
	 */
	public CostVM(int id, int userId, double mips, int numberOfPes, int ram, long bw, long size, String vmm,
			CloudletScheduler cloudletScheduler, double costPerMem, double costPerBW, double costPerStorage,
			double cost, long billing_cycle) {
		this(id, userId, mips, numberOfPes, ram, bw, size, vmm, cloudletScheduler);
		this.costPerMem = costPerMem;
		this.costPerBW = costPerBW;
		this.costPerStorage = costPerStorage;
		this.cost = cost;
		this.billing_cycle = billing_cycle;
		this.vm_start_time = 0;
		this.vm_end_time = Double.MAX_VALUE;
		this.previous_billing_time = 0;
	}

	/**
	 * Creates a new CondorVM object.
	 *
	 * @param id
	 *            unique ID of the VM
	 * @param userId
	 *            ID of the VM's owner
	 * @param mips
	 *            the mips
	 * @param numberOfPes
	 *            amount of CPUs
	 * @param ram
	 *            amount of ram
	 * @param bw
	 *            amount of bandwidth
	 * @param size
	 *            amount of storage
	 * @param vmm
	 *            virtual machine monitor
	 * @param cloudletScheduler
	 *            cloudletScheduler policy for cloudlets
	 * @pre id >= 0
	 * @pre userId >= 0
	 * @pre size > 0
	 * @pre ram > 0
	 * @pre bw > 0
	 * @pre cpus > 0
	 * @pre priority >= 0
	 * @pre cloudletScheduler != null
	 * @post $none
	 */
	public CostVM(int id, int userId, double mips, int numberOfPes, int ram, long bw, long size, String vmm,
			CloudletScheduler cloudletScheduler) {
		super(id, userId, mips, numberOfPes, ram, bw, size, vmm, cloudletScheduler);
		/*
		 * At the beginning all vm status is idle.
		 */
		setState(WorkflowSimTags.VM_STATUS_IDLE);
		/*
		 * If the file.system is LOCAL, we should add a clusterStorage to vm.
		 */
		if (ReplicaCatalog.getFileSystem() == FileSystem.LOCAL) {
			try {
				storage = new ClusterStorage(Integer.toString(id), 1e6);
			} catch (Exception e) {
			}
		}
	}

	/**
	 * @return Gets the state of this VM
	 */
	public int getState() {
		return state;
	}

	/**
	 * @param state
	 *            Sets the state of this VM
	 */
	public void setState(int state) {
		this.state = state;
	}

	/**
	 * @return the costPerMem
	 */
	public double getCostPerMem() {
		return costPerMem;
	}

	/**
	 * @return the costPerBW
	 */
	public double getCostPerBW() {
		return costPerBW;
	}

	/**
	 * @return the costPerStorage
	 */
	public double getCostPerStorage() {
		return costPerStorage;
	}

	/**
	 * @return the cost
	 */
	public double getCost() {
		return cost;
	}

	/**
	 * @return the billing_cycle
	 */
	public long getBilling_cycle() {
		return billing_cycle;
	}

	/**
	 * @param billing_cycle
	 *            the billing_cycle to set
	 */
	public void setBilling_cycle(long billing_cycle) {
		this.billing_cycle = billing_cycle;
	}

	/**
	 * @return the vm_start_time
	 */
	public double getVm_start_time() {
		return vm_start_time;
	}

	/**
	 * @param vm_start_time
	 *            the vm_start_time to set
	 */
	public void setVm_start_time(double vm_start_time) {
		this.vm_start_time = vm_start_time;
	}

	/**
	 * @return the previous_billing_time
	 */
	public double getPrevious_billing_time() {
		return previous_billing_time;
	}

	/**
	 * @param previous_billing_time
	 *            the previous_billing_time to set
	 */
	public void setPrevious_billing_time(double previous_billing_time) {
		this.previous_billing_time = previous_billing_time;
	}

	/**
	 * Adds a file to the local file system
	 *
	 * @param file
	 *            to file to be added to the local
	 * @pre $none
	 * @post $none
	 */
	public void addLocalFile(org.cloudbus.cloudsim.File file) {
		if (this.storage != null) {
			this.storage.addFile(file);
		} else {
		}
	}

	/**
	 * Removes a file from the local file system
	 *
	 * @param file
	 *            to file to be removed to the local
	 * @pre $none
	 * @post $none
	 */
	public void removeLocalFile(org.cloudbus.cloudsim.File file) {
		if (this.storage != null) {
			this.storage.deleteFile(file);
		}
	}

	/**
	 * Tells whether a file is in the local file system
	 *
	 * @return whether the file exists in the local file system
	 * @pre $none
	 * @post $none
	 */
	public boolean hasLocalFile(org.cloudbus.cloudsim.File file) {
		if (this.storage != null) {
			return this.storage.contains(file);
		}
		return false;

	}

	/**
	 * @return the vm_end_time
	 */
	public double getVm_end_time() {
		return vm_end_time;
	}

	/**
	 * @param vm_end_time
	 *            the vm_end_time to set
	 */
	public void setVm_end_time(double vm_end_time) {
		this.vm_end_time = vm_end_time;
	}

}
