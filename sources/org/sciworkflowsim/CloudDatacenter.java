/**
 * 
 */
package org.sciworkflowsim;

import java.util.List;

import org.cloudbus.cloudsim.Datacenter;
import org.cloudbus.cloudsim.DatacenterCharacteristics;
import org.cloudbus.cloudsim.Storage;
import org.cloudbus.cloudsim.VmAllocationPolicy;
import org.cloudbus.cloudsim.core.CloudSim;
import org.cloudbus.cloudsim.core.CloudSimTags;
import org.cloudbus.cloudsim.core.SimEvent;

/**
 * @author stevenbush
 *
 */
public class CloudDatacenter extends Datacenter {

	/**
	 * @param name
	 * @param characteristics
	 * @param vmAllocationPolicy
	 * @param storageList
	 * @param schedulingInterval
	 * @throws Exception
	 */
	public CloudDatacenter(String name, DatacenterCharacteristics characteristics,
			VmAllocationPolicy vmAllocationPolicy, List<Storage> storageList, double schedulingInterval)
			throws Exception {
		super(name, characteristics, vmAllocationPolicy, storageList, schedulingInterval);
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cloudbus.cloudsim.Datacenter#processEvent(org.cloudbus.cloudsim.core.SimEvent)
	 */
	@Override
	public void processEvent(SimEvent ev) {
		int srcId = -1;

		switch (ev.getTag()) {
		// Resource characteristics inquiry
		case CloudSimTags.RESOURCE_CHARACTERISTICS:
			srcId = ((Integer) ev.getData()).intValue();
			sendNow(srcId, ev.getTag(), getCharacteristics());
			break;

		// Resource dynamic info inquiry
		case CloudSimTags.RESOURCE_DYNAMICS:
			srcId = ((Integer) ev.getData()).intValue();
			sendNow(srcId, ev.getTag(), 0);
			break;

		case CloudSimTags.RESOURCE_NUM_PE:
			srcId = ((Integer) ev.getData()).intValue();
			int numPE = getCharacteristics().getNumberOfPes();
			sendNow(srcId, ev.getTag(), numPE);
			break;

		case CloudSimTags.RESOURCE_NUM_FREE_PE:
			srcId = ((Integer) ev.getData()).intValue();
			int freePesNumber = getCharacteristics().getNumberOfFreePes();
			sendNow(srcId, ev.getTag(), freePesNumber);
			break;

		// New Cloudlet arrives
		case CloudSimTags.CLOUDLET_SUBMIT:
			processCloudletSubmit(ev, false);
			break;

		// New Cloudlet arrives, but the sender asks for an ack
		case CloudSimTags.CLOUDLET_SUBMIT_ACK:
			processCloudletSubmit(ev, true);
			break;

		// Cancels a previously submitted Cloudlet
		case CloudSimTags.CLOUDLET_CANCEL:
			processCloudlet(ev, CloudSimTags.CLOUDLET_CANCEL);
			break;

		// Pauses a previously submitted Cloudlet
		case CloudSimTags.CLOUDLET_PAUSE:
			processCloudlet(ev, CloudSimTags.CLOUDLET_PAUSE);
			break;

		// Pauses a previously submitted Cloudlet, but the sender
		// asks for an acknowledgement
		case CloudSimTags.CLOUDLET_PAUSE_ACK:
			processCloudlet(ev, CloudSimTags.CLOUDLET_PAUSE_ACK);
			break;

		// Resumes a previously submitted Cloudlet
		case CloudSimTags.CLOUDLET_RESUME:
			processCloudlet(ev, CloudSimTags.CLOUDLET_RESUME);
			break;

		// Resumes a previously submitted Cloudlet, but the sender
		// asks for an acknowledgement
		case CloudSimTags.CLOUDLET_RESUME_ACK:
			processCloudlet(ev, CloudSimTags.CLOUDLET_RESUME_ACK);
			break;

		// Moves a previously submitted Cloudlet to a different resource
		case CloudSimTags.CLOUDLET_MOVE:
			processCloudletMove((int[]) ev.getData(), CloudSimTags.CLOUDLET_MOVE);
			break;

		// Moves a previously submitted Cloudlet to a different resource
		case CloudSimTags.CLOUDLET_MOVE_ACK:
			processCloudletMove((int[]) ev.getData(), CloudSimTags.CLOUDLET_MOVE_ACK);
			break;

		// Checks the status of a Cloudlet
		case CloudSimTags.CLOUDLET_STATUS:
			processCloudletStatus(ev);
			break;

		// Ping packet
		case CloudSimTags.INFOPKT_SUBMIT:
			processPingRequest(ev);
			break;

		case CloudSimTags.VM_CREATE:
			processVmCreate(ev, false);
			break;

		case CloudSimTags.VM_CREATE_ACK:
			processVmCreate(ev, true);
			break;

		case CloudSimTags.VM_DESTROY:
			processVmDestroy(ev, false);
			break;

		case CloudSimTags.VM_DESTROY_ACK:
			processVmDestroy(ev, true);
			break;

		case CloudSimTags.VM_MIGRATE:
			processVmMigrate(ev, false);
			break;

		case CloudSimTags.VM_MIGRATE_ACK:
			processVmMigrate(ev, true);
			break;

		case CloudSimTags.VM_DATA_ADD:
			processDataAdd(ev, false);
			break;

		case CloudSimTags.VM_DATA_ADD_ACK:
			processDataAdd(ev, true);
			break;

		case CloudSimTags.VM_DATA_DEL:
			processDataDelete(ev, false);
			break;

		case CloudSimTags.VM_DATA_DEL_ACK:
			processDataDelete(ev, true);
			break;

		case CloudSimTags.VM_DATACENTER_EVENT:
			updateCloudletProcessing();
			checkCloudletCompletion();
			break;

		// other unknown tags are processed by this method
		default:
			processOtherEvent(ev);
			break;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cloudbus.cloudsim.Datacenter#processVmCreate(org.cloudbus.cloudsim.core.SimEvent, boolean)
	 */
	@Override
	protected void processVmCreate(SimEvent ev, boolean ack) {
		// TODO Auto-generated method stub
		CostVM vm = (CostVM) ev.getData();
		double current_time = CloudSim.clock();
		vm.setVm_start_time(current_time);
		super.processVmCreate(ev, ack);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cloudbus.cloudsim.Datacenter#processVmDestroy(org.cloudbus.cloudsim.core.SimEvent, boolean)
	 */
	@Override
	protected void processVmDestroy(SimEvent ev, boolean ack) {
		// TODO Auto-generated method stub
		CostVM vm = (CostVM) ev.getData();
		double current_time = CloudSim.clock();
		vm.setVm_end_time(current_time);
		super.processVmDestroy(ev, ack);
	}

	/**
	 * Updates processing of each VM running in this Datacenter. Checking if a VM complete its billing cycle, if yes,
	 * require the budget for next billing cycle, if no, destroy this VM.
	 */
	protected void updateVMProcessing() {

	}

}
